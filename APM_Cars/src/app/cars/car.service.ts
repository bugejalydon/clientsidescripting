import { Injectable } from "@angular/core";
import { ICar } from "./cars";

@Injectable()
export class CarService{
    getCars(): ICar[]{
        return [
        {
            "carId": 1, 
            "model": "KIA Ceed", 
            "price": 10000
        },
        {
            "carId": 2, 
            "model": "Maruti Gypsy", 
            "price": 8000.987
        }, 
        {
            "carId": 3, 
            "model": "Toyota Vitz", 
            "price": 5000
        }
    ]
    }
}