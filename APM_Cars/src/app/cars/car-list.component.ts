import { Component } from "@angular/core";
import { ICar } from "./cars";
import { CarService } from "./car.service";

@Component({
    selector: 'pm-cars', 
    templateUrl: './cars-list.component.html'
})

export class CarListComponent {
    title: string = 'List of Cars'; 
    _filter: string = 'All'; 
    filteredCars : ICar[]; 
    get filter(): string{
        return this._filter; 
    }

    set filter(value: string){
        this._filter = value; 
        this.filteredCars = this._filter ? this.performFilter(this.filter) : this.cars; 
    }

    cars: ICar[] = []; 

    constructor(private _carService : CarService){
        this.filter = "All Products"; 
        
    }

    ngOnInit():void{
        this.cars = this._carService.getCars(); 
        this.filteredCars = this.cars; 
    }

    performFilter(filterBy: string) : ICar[]{
        filterBy = filterBy.toLocaleLowerCase(); 
        return this.cars.filter((car:ICar)=>car.model.toLocaleLowerCase().indexOf(filterBy) !==-1); 
    }
}

