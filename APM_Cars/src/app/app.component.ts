import { Component } from '@angular/core';
import { CarService } from './cars/car.service';

@Component({
  selector: 'pm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'], 
  providers: [CarService]
})
export class AppComponent {
  title = 'Angular: Getting Started';
}
