export class Joke{
    setup: string; 
    punchline: string; 
    hide: boolean; 

    constructor(setup, punchline){
        this.setup = setup; 
        this.punchline = punchline
        this.hide = true; 
    }
    toggleHide(){
        this.hide = !this.hide; 
    }
}