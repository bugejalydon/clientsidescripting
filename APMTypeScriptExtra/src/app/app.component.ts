import { Component } from '@angular/core';
import {Joke} from "./joke"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'APMTypeScriptExtra';
  jokes: Joke[]; 
  hide: boolean = false; 

  constructor(){
    this.jokes=[
      new Joke("What did the cheese say when it looked in the mirror?", "Hello-Me (Halloumi)"), 
      new Joke("What kind of cheese do you use to disguise a small horse?", "Mask-a-pony (Mascarpone)"), 
      new Joke("A kid threw a lump of cheddar at me", "I tought 'That's not very mature'")
    ];
  }

  
}


