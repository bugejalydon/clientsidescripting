import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { ProductListComponent } from './products/product-list.component';
import {birthdayComponent} from './BirthdayExample/birthdayExample.component'; 
import {ConvertToSpacesPipe} from './shared/convert-to-spaces.pipe'; 

@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    birthdayComponent, 
    ConvertToSpacesPipe
  ],
  imports: [
    BrowserModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent] 
})
export class AppModule { }
